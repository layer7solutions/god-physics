

def gravity(falling=True):
    if falling:
        dont_fall()


def dont_fall():
    return False


if __name__ == "__main__":
    alive = True
    while alive:
        gravity()
